# README #

Welcome to the Open Source repository of iLocalize. This repository contains the source code of iLocalize which has been retired but open-sourced for the community under the BSD license (see license.txt).

Thank you!

Jean Bovet
Arizona Software